
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
" alternatively, pass a path where Vundle should install plugins
"let path = '~/some/path/here'
"call vundle#rc(path)

" let Vundle manage Vundle, required
Plugin 'gmarik/vundle'
Plugin 'https://github.com/ervandew/supertab.git'
Plugin 'https://github.com/davidhalter/jedi-vim.git'

Bundle 'chase/vim-ansible-yaml'


filetype plugin indent on
filetype plugin on


" jedi-vim
" let g:jedi#use_tabs_not_buffers = 0

let g:jedi#auto_initialization = 0


set autoindent 
set tabstop=4    
set shiftwidth=4    
set expandtab    

set hlsearch 

colorscheme koehler

imap ,, <ESC>

imap ,y ---<Enter><Enter>- name: 
imap ,n - name: 
imap ,i {{ item }}
imap ,o owner=root group=root mode=0644


imap ,c <Space><Space>copy: src=
imap ,d <Enter><Space><Space><Space><Space><Space><Space>dest=


"insert and remove comments in visual and normal mode
vmap ,ic :s/^/#/g<CR>:let @/ = ""<CR>
map  ,ic :s/^/#/g<CR>:let @/ = ""<CR>
vmap ,rc :s/^#//g<CR>:let @/ = ""<CR>
map  ,rc :s/^#//g<CR>:let @/ = ""<CR>
